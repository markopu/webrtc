var app = {
    init: function(config) {
	/* 
	 * Initialize SimpleWebRTC and add necessary event listeners
	 */

	var servers = new Array();

	for (server in config.stunServers) {
	    servers.push({'url': 'stun:' + config.stunServers[server]});
	}

	for (server in config.turnServers) {
	    var serverData = config.turnServers[server].split(",");
	    servers.push({
		'url': 'turn:' + serverData[0],
		'credential': serverData[1],
		'username': serverData[2]
	    });
	}

	webrtc = new SimpleWebRTC({
	    url: config.signallingServer,
	    localVideoEl: 'localVideo',
	    remoteVideosEl: 'remoteVideos',
	    autoRequestMedia: true,
	    peerConnectionConfig: {
		iceServers: [
		    servers
		]
	    }
	});

	webrtc.on('connectionReady', function(session_id) {
	    console.log("Connection ready");
	});

	// immediately pause video 
	webrtc.on('readyToCall', function () {
	    webrtc.pause();	
	});

	// Allow user to connect when application loads
	$("#joinBtn").removeClass("ui-disabled");

	/*
	 * Peer handling
	 */
	webrtc.on('createdPeer', function (peer) {

	    // Incomming file handling
	    ph.addIncomingFileHandler(peer, "#downloads");

	    // Outgoing file handling
	    ph.addOutgoingFileHandler(peer, "transferStatus", "avaliablePeers");
	    
	    // add peer id to the Participants list
	    var audioParticipantsList = document.getElementById("audioConferenceParticipants");
	    var entry = document.createElement("li");
	    entry.id = "audio_" + peer.id;
	    entry.innerHTML = peer.id;
	    audioParticipantsList.appendChild(entry);
	});


	/*
	 * Video conference
	 */
	webrtc.on('videoAdded', function (video, peer) {
	    console.log('video added', peer);
	    var node = document.getElementById("remoteVideos");
	    video.id = "video_" + peer.id;
	    node.appendChild(video);


	    /*
	     * DEBUG - display connection releated data
	     */

	    /*
	    var rtcPeerConn = peer.pc.pc.peerconnection;
	    console.log(rtcPeerConn);

	    var previousBytes = 0;
	    var updateDbgInfo = setInterval(function () {
		rtcPeerConn.getStats(function (connStats) {
		    var rtcStatsReports = connStats.result();


		    var bytesReceived = rtcStatsReports[5].stat('bytesReceived');
		    var bps = bytesReceived - previousBytes;
		    previousBytes = bytesReceived;
		    
		    // current bandwidth usage - received
		    console.log(bps)
		});
	    }, 1000);

	    */
	});

	webrtc.on('videoRemoved', function (video, peer) {
	    console.log('video removed', peer);
	    var node = document.getElementById("remoteVideos");
	    if(!document.getElementById("video_" + peer) === "video_")
		node.removeChild(document.getElementById("video_" + peer));

	    // remove file input as well
	    var filecontainer = document.getElementById("input_" + peer.id);

	    if (filecontainer)
		document.getElementById("avaliablePeers").removeChild(filecontainer);

	    // remove peer from audio participants list
	    var audioPeer = document.getElementById("audio_" + peer.id);
	    if (audioPeer) 
		document.getElementById("audioConferenceParticipants").removeChild(audioPeer);
	});


	/*
	 * Chat
	 */
	webrtc.connection.on('message', function (data) {
	    if (data.type === 'chat') {
		console.log('chat received', data);
		$("#messagewindow").append("<p>"+ data.from + ": " + data.payload + "</p>");
		scrollChatToBottom();
	    }
	});


	/*
	 * File download handling
	 */
	$("#downloads").on('tap', '.navlink', function (e) {
	    console.log("tap");
	    var uriString = $(this).data('url');
	    var fileName = $(this).attr('id');

	    var fileTransfer = new FileTransfer();
	    var fileURL = cordova.file.externalDataDirectory;
	    
	    // request file system access
	    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
	    
	    function gotFS(fileSystem) {
		fileSystem.root.getFile(fileName,
					{create: true, exclusive: false},
					gotFileEntry, fail);
	    }

	    function fail(err) {
		console.log(err);
	    }

	    function gotFileEntry(fileEntry) {
		fileEntry.createWriter(gotFileWriter, fail);
	    }

	    function gotFileWriter(writer) {
		writer.seek(0);

		// get binary blob that has been passed with data URI
		var decodedData = getBlob(uriString);

		writer.write(decodedData);

		function alertDismissed() {
		}
		
		function fileOpened(data) {
		}

		function confirmOpen(btnIndex) {
		    if (btnIndex == 1) {
			// we open file
			window.cordova.plugins.FileOpener.openFile(
			    cordova.file.externalRootDirectory + '/' + fileName,
			    fileOpened,
			    errCb);
		    }
		}
		
		function sucessCb(data) {
		    console.log(data);
		    // prompt user if they want to open file
		    navigator.notification.confirm("Do you want to open " + fileName + "?",
						   confirmOpen, "Open file", ["Yes", "No"]);
		}

		function errCb(err) {
		    console.log(err);
		    navigator.notification.alert(
			'This filetype extension is not supported. File has been saved on your sdcard.',
			alertDismissed,
			'Error',
			'Ok');
		}

		// check if user can open file with installed app
		window.cordova.plugins.FileOpener.canOpenFile(
		    cordova.file.externalRootDirectory + '/' + fileName,
		    sucessCb,
		    errCb);
	    }


	});
    },

    reload: function() {
	location.reload();
    },


    /*
     * Input: config file name, callback
     * Output: Array[signalling server, [stun servers], [turn servers]]
     */
    loadConfig: function(configFileName, cb) {
	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
	
	function gotFS(fileSystem) {
	    fileSystem.root.getFile(configFileName,
				    {create: true, exclusive: false},
				    gotFileEntry, fail);
	}

	function fail(err) {
	    console.log(err);
	}

	function gotFileEntry(fileEntry) {
	    fileEntry.file(function (file) {

		var reader = new FileReader();

		reader.onloadend = function (e) {

		    var lines = this.result.split('\n');
		    
		    // check if file is empty
		    if (lines.length == 1 && lines[0] == "") {
			fileEntry.createWriter(gotWriter, fail);
		    } else {
			// we read file and return parameters
			// read signalling server
			
			var i = 0;
			var signalling = '';
			var stun = new Array();
			var turn = new Array();

			var addSignalling = false;
			var addSTUN = false;
			var addTURN = false;

			for (var i = 0; i < lines.length; i++) {
			    switch (lines[i]) {
			    case "SIGNALLING:":
				i++;
				addSignalling = true;
				break;
			    case "STUN:":
				i++;
				addSTUN = true;
				break;
			    case "TURN:":
				i++;
				addTURN = true;
				addSTUN = false;
				break;
			    default:
				break;
			    }

			    if (addSignalling == true) {
				signalling = lines[i];
				addSignalling = false;
			    }

			    if (addSTUN == true) {
				if (lines[i] != "")
				    stun.push(lines[i]);
			    }

			    if (addTURN == true) {
				if (lines[i] != "")
				    turn.push(lines[i]);
			    }
			}

			var result = {
			    signallingServer: signalling,
			    stunServers: stun,
			    turnServers: turn
			}
			
			cb(result);
		    }
		};

		reader.readAsText(file);
	    });

	}

	function gotWriter(writer) {
	    writer.seek(0);
	    
	    var defaultConfig = 'SIGNALLING:\nhttp://79.133.43.107:8888\n\nSTUN:\n79.133.43.107:3478\n\nTURN:\n79.133.43.107:3478?transport=udp,testtest,echo\n79.133.43.107:3478?transport=tcp,testtest,echo\n'

	    // file is empty, create default config
	    var blob = new Blob([defaultConfig], {type: 'text/plain'});
	    
	    writer.onwriteend = function() {
		// ask app to load config again, since we created one
		app.loadConfig(configFileName, cb);
	    };
	    
	    writer.write(blob);
	}
	
    },

    writeConfig: function(configFileName, config) {
	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
	
	function gotFS(fileSystem) {
	    fileSystem.root.getFile(configFileName,
				    {create: true, exclusive: false},
				    gotFileEntry, fail);
	}

	function fail(err) {
	    console.log(err);
	}

	function gotFileEntry(fileEntry) {
	    fileEntry.createWriter(gotWriter, fail);
	}

	function gotWriter(writer) {
	    var text_to_write = "";

	    text_to_write += "SIGNALLING:\n";
	    text_to_write += config.signallingServer + "\n\n";

	    text_to_write += "STUN:\n";
	    for (url in config.stunServers) {
		text_to_write += config.stunServers[url] + "\n";
	    }
	    text_to_write += "\n";

	    text_to_write += "TURN:\n";
	    for (url in config.turnServers) {
		text_to_write += config.turnServers[url] + "\n";
	    }
	    text_to_write += "\n";

	    var blob = new Blob([text_to_write], {type: 'text/plain'});

	    writer.onwriteend = function() {
		if (writer.length === 0) {
		    writer.write(blob);
		} else {

		}
	    }
	    writer.truncate(0);
	}

    },

    deleteConfiguration: function(conf_name) {
	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

	function fail(err) {
	    console.log(err);
	}

	function gotFS(fileSystem) {
	    fileSystem.root.getFile(conf_name, {create: false}, function (fileEntry) {
		fileEntry.remove(function () {
		    console.log("Removed default config");
		    location.reload();
		    $.mobile.navigate("#");
		}, fail);
	    }, fail);
	}
    },
}

// TODO: Move this somewhere else
function getBlob(dataURL) {
    var BASE64_MARKER = ';base64,';
    if (dataURL.indexOf(BASE64_MARKER) == -1) {
	var parts = dataURL.split(',');
	var contentType = parts[0].split(':')[1];
	var raw = decodeURIComponent(parts[1]);

	return new Blob([raw], {type: contentType});
    }

    var parts = dataURL.split(BASE64_MARKER);
    var contentType = parts[0].split(':')[1];
    var raw = window.atob(parts[1]);
    var rawLength = raw.length;

    var uInt8Array = new Uint8Array(rawLength);

    for (var i = 0; i < rawLength; ++i) {
	uInt8Array[i] = raw.charCodeAt(i);
    }

    return new Blob([uInt8Array], {type: contentType});
}
