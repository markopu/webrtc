/*
 * Peer handling
 */

var ph = {

    /* Peer incoming file handler
     * Inputs: WebRTC peer object, id of downloads <ul> element
     * Outputs: null
     */
    addIncomingFileHandler: function (peer, id) {
	peer.on('fileTransfer', function (metadata, receiver) {

	    receiver.on('progress', function (bytesReceived) {
		// TO DO: Add progress bar to indicade that file
		// transfer is in progress
	    });
	    
	    // Successfully received file
	    receiver.on('receivedFile', function (file, metadata) {

		var f = new FileReader();

		// append file URI to avaliable downloads section
		f.onload = function (event) {
		    $(id).append("<li><a href='#' class='navlink' id='" +
					   metadata.name +
					   "' data-url='" + 
					   f.result + "'>" + 
					   metadata.name +
					   "</a></li>");
		};

		f.readAsDataURL(file);
		
		receiver.channel.close();
	    });
	});
    },

    /* Peer outgoing file handler
     * Inputs: WebRTC peer object, progress container element id, avaliavle peers
     * elements id
     * Outputs: null
     */
    addOutgoingFileHandler: function (peer, id_progress, id_avaliable) {

	var fileinputContainer = document.createElement('div');
	var peerName = document.createElement('h3');

	var statusContainer = document.getElementById(id_progress);
	peerName.innerHTML = peer.id + ':';
	fileinputContainer.id = 'input_' + peer.id;
	
	var fileinput = document.createElement('input');
	fileinput.type = 'file';
	
	fileinput.addEventListener('change', function () {
	    var sendProgress = document.createElement('progress');
	    var file = fileinput.files[0];
	    var sender = peer.sendFile(file);

	    sendProgress.max = file.size;
	    fileinputContainer.appendChild(sendProgress);
	    fileinput.disabled = true;

	    sender.on('progress', function (bytesSent) {
		sendProgress.value = bytesSent;
	    });
	    
	    sender.on('sentFile', function () {
		// file was sent sucessefully
		fileinput.removeAttribute('disabled');
		// remove file from input
		fileinput.value = "";
		fileinputContainer.removeChild(sendProgress);
		statusContainer.innerHTML = 'Successfuly sent file '
					  + file.name + ' to peer ' + peer.id;
		
	    });
	});

	fileinputContainer.appendChild(peerName);
	fileinputContainer.appendChild(fileinput);

	document.getElementById(id_avaliable).appendChild(fileinputContainer);
    }


}
