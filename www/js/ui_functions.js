/*
 * Room UI handling
 */
function promptRoom() {
    navigator.notification.prompt("Please enter your room name: ",
				  joinRoom, 
				  "Room name");
}

function joinRoom(results) {
    var btnIndex = results.buttonIndex;
    // ok == 1, cancel == 2
    if (btnIndex === 1) {
	document.getElementById("roomName").innerHTML = 'Room name: ' + results.input1;
	$("#joinBtn").addClass("ui-disabled");

	webrtc.joinRoom(results.input1, function(err) {
	    console.log("ok");
	    console.log(err);
	    if (!err) {
		$("#leaveBtn").removeClass("ui-disabled");
		$("#audioConf").removeClass("ui-disabled");
		$("#videoConf").removeClass("ui-disabled");
		$("#textChat").removeClass("ui-disabled");
		$("#fileTransfer").removeClass("ui-disabled");
	    }
	});
	console.log("Joined room: " + results.input1);
    }
}

function leaveRoom() {
    webrtc.leaveRoom();
    console.log("left room");
    document.getElementById("roomName").innerHTML = "";
    $("#joinBtn").removeClass("ui-disabled");
    $("#leaveBtn").addClass("ui-disabled");
    $("#audioConf").addClass("ui-disabled");
    $("#videoConf").addClass("ui-disabled");
    $("#textChat").addClass("ui-disabled");
    $("#fileTransfer").addClass("ui-disabled");
}

/*
 * Video conference UI handling
 */
function beginVideoConference() {
    webrtc.setVolumeForAll(1);
    webrtc.resume();
}

// stop video when user leaves video conference
$(document).on("pagebeforehide", "#videoConference", function() {
    webrtc.pause();
    webrtc.setVolumeForAll(0);
});


/*
 * Audio conference UI handlig
 */
function beginAudioConference() {
    webrtc.setVolumeForAll(1);
    webrtc.resume();
    webrtc.pauseVideo();
}

// audio pausing when user leaves audio conference
$(document).on("pagebeforehide", "#audioConference", function() {
    webrtc.pause();
    webrtc.setVolumeForAll(0);
});


/*
 * Text chat UI handling
 */
function beginTextChat() {
    scrollChatToBottom();
}

function sendText() {
    var message = $("#usermessage").val();
    webrtc.sendToAll('chat', message);
    $("#messagewindow").append("<p>You: " + message + "</p>");
    $("#usermessage").val('');
    scrollChatToBottom();
}

function scrollChatToBottom() {
    var objDiv = document.getElementById("messagewindow");
    objDiv.scrollTop = objDiv.scrollHeight;
}

/*
 * File transfer UI handling
 */
function beginSendFiles() {
}

/*
 * Debug UI handling
 */
function reloadApp() {
    location.reload();
}

/*
 * Configuration UI handling
 */
function configureSettings() {
    // empty any previous entries
    $("#stun_placeholder").empty();
    $("#turn_placeholder").empty();
    app.loadConfig('app.conf', loadingDone);
    
    function loadingDone(config) {
	// load parameters in config page
	loadParametersUI(config, "#signalling_server", "#stun_placeholder", "#turn_placeholder");

	// update content on page
	$("#configureSettings").trigger('create');
    }
}


// load parameters to config fields
function loadParametersUI(config, sigSrv, stunSrv, turnSrv) {
    $(sigSrv).val(config.signallingServer);

    for (url in config.stunServers) {
	
	$(stunSrv).append("<div id='stun_" + url + "'></div>");
	
	$("#stun_" + url).append("<div class='addr_input'><input type='text' id='stun_" + 
				  url + "' value='" + 
				  config.stunServers[url] + "'></div>");
	$("#stun_" + url).append("<div class='delete_btn'><button onClick='deleteStunEntry(" +
				 url + ")'>X</button></div>");

    }

    for (url in config.turnServers) {
	$(turnSrv).append("<div id='turn_" + url + "'></div>");

	$("#turn_" + url).append("<div class='addr_input'><input type='text' id='turn_" + 
				  url + "' value='" + 
				  config.turnServers[url] + "'></div>");
	$("#turn_" + url).append("<div class='delete_btn'><button onClick='deleteTurnEntry(" + 
			  url + ")'>X</button></div>");
    }

}

// delete entries without saving
function deleteStunEntry(n) {
    $("#stun_" + n).remove();
}

function deleteTurnEntry(n) {
    $("#turn_" + n).remove();
}

function inputParametersToConfig() {
    console.log($("#stun_servers").find("input"));
}

function saveConfiguration() {
    var signalling_server = $("#sig_server").find("input")[0].value;
    var stun_servers = $("#stun_servers").find("input");
    var turn_servers = $("#turn_servers").find("input");

    var stun_urls = new Array();
    var turn_urls = new Array();

    for (var i = 0; i < stun_servers.length; i++) {
	stun_urls.push(stun_servers[i].value);
    }

    for (var i = 0; i < turn_servers.length; i++) {
	turn_urls.push(turn_servers[i].value);
    }

    var result = {
	signallingServer: signalling_server,
	stunServers: stun_urls,
	turnServers: turn_urls
    }

    app.writeConfig("app.conf", result);
    
    $.mobile.navigate("#");
}

function addSTUNServer() {
    navigator.notification.prompt("Enter url and port number of STUN server:",
				  addServerCb);

    function addServerCb(results) {
	var url = results.input1;

	var n = document.getElementById("stun_placeholder").children.length;

	$("#stun_placeholder").append("<div id='stun_" + n + "'></div>");
	$("#stun_" + n).append("<div class='addr_input'><input type='text' id='stun_" + 
				  n + "' value='" + 
				  url + "'></div>");
	$("#stun_" + n).append("<div class='delete_btn'><button onClick='deleteStunEntry(" +
				 n + ")'>X</button></div>");

	// tell jQuery to update page
	$("#configureSettings").trigger('create');
    }
}

function addTURNServer() {
    navigator.notification.prompt("Enter url, port number, password, and username of TURN server:",
				  addServerCb);

    function addServerCb(results) {
	var url = results.input1;

	var n = document.getElementById("turn_placeholder").children.length;

	$("#turn_placeholder").append("<div id='turn_" + n + "'></div>");

	$("#turn_" + n).append("<div class='addr_input'><input type='text' id='turn_" + 
				  n + "' value='" + 
				  url + "'></div>");
	$("#turn_" + n).append("<div class='delete_btn'><button onClick='deleteTurnEntry(" + 
			  n + ")'>X</button></div>");

	// tell jQuery to update page
	$("#configureSettings").trigger('create');
    }
}

function useDefaultConfiguration() {
    app.deleteConfiguration("app.conf");
}
